import json
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee
from django.views.decorators.http import require_http_methods
from events.models import Conference


class AttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "conference",
    ]

    def get_extra_data(self, o):
        return {
            "created": o.created,
            "conference": {
                "name": o.conference.name,
                "href": o.conference.get_api_url(),
            },
        }


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_id)
        attendees_list = [
            AttendeeEncoder().default(attendee)
            for attendee in attendees
        ]
        return JsonResponse({"attendees": attendees_list}, encoder=AttendeeEncoder)
    else:
        content = json.loads(request.body)
        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            attendee = Attendee.objects.get(id=id)
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid attendee id"},
                status=400,
            )

        attendee.name = content.get("name", attendee.name)
        attendee.email = content.get("email", attendee.email)
        attendee.company_name = content.get(
            "company_name", attendee.company_name
            )

        attendee.save()

        return JsonResponse(
            attendee,
            encoder=AttendeeEncoder,
            safe=False,
        )
