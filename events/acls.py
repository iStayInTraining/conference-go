import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # use pexels api
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}"
        }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    # Create the URL for the geocoding API with the city, state, and country
    geocoding_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"

    # Make the request to the geocoding API
    geocoding_response = requests.get(geocoding_url)
    geocoding_data = geocoding_response.json()

    if not geocoding_data:
        # Geocoding data not available, return None
        return None

    try:
        # Get the latitude and longitude from the geocoding response
        latitude = geocoding_data[0]["lat"]
        longitude = geocoding_data[0]["lon"]
    except (KeyError, IndexError):
        # Error occurred while retrieving latitude and longitude, return None
        return None

    # Create the URL for the current weather API with the latitude, longitude, and API key
    weather_url = f"http://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}"

    # Make the request to the current weather API
    weather_response = requests.get(weather_url)
    weather_data = weather_response.json()

    if "main" not in weather_data or "weather" not in weather_data:
        # Weather data not available, return None
        return None

    try:
        # Get the main temperature and weather description from the weather response
        temperature = weather_data["main"]["temp"]
        weather_description = weather_data["weather"][0]["description"]
    except (KeyError, IndexError):
        # Error occurred while retrieving temperature and description, return None
        return None

    # Convert temperature from Kelvin to Fahrenheit
    temperature_fahrenheit = (temperature - 273.15) * 9 / 5 + 32

    # Create a dictionary with the weather data
    weather_dict = {
        "temperature": temperature_fahrenheit,
        "description": weather_description
    }

    # Return the weather dictionary
    return weather_dict
